package com.tw.tsm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.tw.tsm.*"})
@ComponentScan(value = {"com.tw.tsm.*"})
public class GwApplication {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        SpringApplication.run(GwApplication.class);
    }
}