package com.tw.tsm.cipher.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.tw.tsm.feign.api.cipher.CipherFeignClient;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@RestController
@Slf4j
public class EchoController implements CipherFeignClient
{
    
    public String echo(@PathVariable String string)
    {
        ServletRequestAttributes attributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        String tId = request.getHeader("tId");
        log.info("=============tId:{}", tId);
        return "Hello Nacos Discovery " + string;
    }
    
}
