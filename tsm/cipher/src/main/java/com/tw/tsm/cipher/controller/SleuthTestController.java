package com.tw.tsm.cipher.controller;

import com.tw.tsm.base.MdcContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class SleuthTestController
{
    
    @RequestMapping(value = "/echo/{str}", method = RequestMethod.GET)
    public void echo(@PathVariable String str)
    {
        log.info("echo init begin..........tId：{}", MdcContext.getTraceId());
    }
}
