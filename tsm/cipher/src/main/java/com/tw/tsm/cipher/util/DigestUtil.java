package com.tw.tsm.cipher.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class DigestUtil
{
    /** MD5 */
    public static final String MD5 = "MD5";
    
    /** SHA-1 */
    public static final String SHA_1 = "SHA-1";
    
    /**
     * 对字符串md5加密
     * @param data 待加密数据
     * @return
     */
    public static String md5Digest(String data)
    {
        return digestByAlgorithm(MD5, data);
    }
    
    /**
     * 对字符串sha1加密
     * @param data 待加密数据
     * @return
     */
    public static String sha1Digest(String data)
    {
        return digestByAlgorithm(SHA_1, data);
    }
    
    /**
     * 根据指定算法加密
     * @param algorithm 算法
     * @param data 待加密数据
     * @return
     */
    public static String digestByAlgorithm(String algorithm, String data)
    {
        // 为空不处理
        if (data == null)
        {
            return null;
        }
        try
        {
            // 指定摘要算法的MessageDigest对象
            MessageDigest md = MessageDigest.getInstance(algorithm);
            
            // 使用指定的字节更新摘要
            md.update(data.getBytes());
            
            // BigInteger函数则将8位的字符串转换成16位hex值，用字符串来表示；得到字符串形式的hash值
            return new BigInteger(1, md.digest()).toString(16);
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public static void main(String[] args)
    {
        String md1 = md5Digest("hello world");
        String md2 = md5Digest("hallo world");
        System.out.println(md1);
        System.out.println(md2);

    }
}
