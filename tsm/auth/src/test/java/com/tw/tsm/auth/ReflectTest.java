package com.tw.tsm.auth;

import java.lang.reflect.Method;


import com.tw.tsm.auth.dto.request.GetTokenReq;
import org.junit.jupiter.api.Test;

public class ReflectTest
{
    
    @Test
    public void test()
    {
        // 获取当前类的Class对象
        Class clazz = GetTokenReq.class;
        System.out.println("clazz:" + clazz.getName());
        
        // 获取父类
        Class superClass = clazz.getSuperclass();
        System.out.println("superClass:" + superClass.getName());
        
        // 父类上的所有方法
        Method[] methods = superClass.getDeclaredMethods();
        for (Method method : methods)
        {
            System.out.println("method:" + method.getName());
        }
        
    }
}
