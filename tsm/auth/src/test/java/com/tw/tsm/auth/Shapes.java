package com.tw.tsm.auth;

import java.util.Arrays;

interface Line
{
}

abstract class Shape implements Line
{
    void draw()
    {
        System.out.println(this + ".draw()");
    }
    
    abstract public String toString();
}

class Circle extends Shape
{
    @Override
    public String toString()
    {
        return "Circle";
    }
}

class Square extends Shape
{
    @Override
    public String toString()
    {
        return "Square";
    }
}

class Triangle extends Shape
{
    @Override
    public String toString()
    {
        return "Triangle";
    }
}

public class Shapes
{
    public static void main(String[] args)
    {
        try
        {
            // 加载Circle类
            Class clazz = Class.forName("com.tw.tsm.auth.Circle");
            printInfo(clazz);
            
            // 实例化对象
            Object superObj = clazz.newInstance();
            printInfo(superObj.getClass());
            
            // 获取父类-Shape
            Class superClazz = clazz.getSuperclass();
            printInfo(superClazz);
            
            // 获取父类上所有实现的接口Line
            Arrays.stream(superClazz.getInterfaces()).forEach(f -> {
                printInfo(f);
            });

            Class integerClass = int.class;
            printInfo(integerClass);
        }
        catch (ClassNotFoundException | InstantiationException | IllegalAccessException e)
        {
            e.printStackTrace();
        }
    }
    
    public static void printInfo(Class clazz)
    {
        // 是否接口
        System.out.println("Class Name: " + clazz.getName() + " is interface?[" + clazz.isInterface() + "]");
        // 对象名称
        System.out.println("Simple name: " + clazz.getSimpleName());
        // 对象全限定路径名称
        System.out.println("Canonical name: " + clazz.getCanonicalName());
        System.out.println("=================================================");
    }
}
