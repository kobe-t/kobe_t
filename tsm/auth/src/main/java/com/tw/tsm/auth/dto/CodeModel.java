package com.tw.tsm.auth.dto;

public class CodeModel {

    /** 用户ID */
    private String userId;

    /** 应用ID */
    private String clientId;

    /** 授权范围 */
    private String scope;

    /** 授权码 */
    private String code;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
