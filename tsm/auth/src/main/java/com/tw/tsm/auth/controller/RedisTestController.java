package com.tw.tsm.auth.controller;

import com.tw.tsm.base.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/redis")
public class RedisTestController
{
    
    @Autowired
    private RedisUtil redisUtil;
    
    @GetMapping("/set")
    public void testSet(@RequestParam("key") String key, @RequestParam("val") String val)
    {
        redisUtil.set(key, val);
    }
    
    @GetMapping("/get")
    public String testGet(@RequestParam("key") String key)
    {
        return (String)redisUtil.get(key);
    }
}
