//package com.tw.tsm.auth.feign;
//
//import com.tw.tsm.feign.api.cipher.AuthFeignClientFallback;
//import org.springframework.cloud.openfeign.FeignClient;
//
//import com.tw.tsm.base.constant.FeignConstant;
//import com.tw.tsm.feign.api.cipher.CipherFeignClient;
//
//@FeignClient(name = FeignConstant.CIPHER_SERVICE, fallback = AuthFeignClientFallback.class)
//public interface AuthFeignClient extends CipherFeignClient
//{
//}
