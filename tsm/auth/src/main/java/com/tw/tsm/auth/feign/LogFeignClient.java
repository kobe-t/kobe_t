package com.tw.tsm.auth.feign;

import com.tw.tsm.base.constant.FeignConstant;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = FeignConstant.LOG_SERVICE, fallback = LogFeignClientFallback.class)
public interface LogFeignClient
{
    @GetMapping(value = "/log/echo/{str}")
    String echo(@PathVariable("str") String str);
}
