package com.tw.tsm.auth.feign;

import org.springframework.stereotype.Component;

@Component
public class LogFeignClientFallback implements LogFeignClient {

    @Override
    public String echo(String str) {
        return "fail";
    }
}
