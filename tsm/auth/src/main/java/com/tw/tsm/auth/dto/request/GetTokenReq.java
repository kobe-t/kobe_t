package com.tw.tsm.auth.dto.request;

import com.tw.tsm.auth.dto.BaseAuthReq;

public class GetTokenReq extends BaseAuthReq
{
    
    /** 授权码类型 */
    private String grantType;
    
    /** 授权码 */
    private String code;
    
    public String getGrantType()
    {
        return grantType;
    }
    
    public void setGrantType(String grantType)
    {
        this.grantType = grantType;
    }
    
    public String getCode()
    {
        return code;
    }
    
    public void setCode(String code)
    {
        this.code = code;
    }
}
