package com.tw.tsm.auth.dto.request;

import com.tw.tsm.base.request.BaseRequestDTO;
import lombok.Data;

@Data
public class AuthorizeReq extends BaseRequestDTO
{
    
    private String responseType;
    
    private String clientId;
    
    private String redirectUri;
    
    private String scope;
}
