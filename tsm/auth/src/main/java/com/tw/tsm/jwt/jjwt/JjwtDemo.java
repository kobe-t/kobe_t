package com.tw.tsm.jwt.jjwt;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.security.Key;
import java.security.KeyPair;

public class JjwtDemo
{
    
    public static void main(String[] args)
    {
        // 生成jwt令牌
        Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
        String jwtToken = Jwts.builder().setSubject("Joe").signWith(key).compact();
        System.out.println("jwtToken:" + jwtToken);

        // 检验jwt令牌
        String name = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(jwtToken).getBody().getSubject();
        System.out.println("subject:" + name);

        // 密钥对
        KeyPair keyPair = Keys.keyPairFor(SignatureAlgorithm.RS256);
        // 私钥加密
        String jwtToken2 = Jwts.builder().setSubject("James").signWith(keyPair.getPrivate()).compact();
        System.out.println("jwtToken2:" + jwtToken2);

        // 检验jwt令牌,公钥解密
        String name2 = Jwts.parserBuilder().setSigningKey(keyPair.getPublic()).build().parseClaimsJws(jwtToken2).getBody().getSubject();
        System.out.println("subject:" + name2);
    }


}
