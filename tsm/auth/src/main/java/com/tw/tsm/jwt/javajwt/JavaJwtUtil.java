package com.tw.tsm.jwt.javajwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;

public class JavaJwtUtil
{
    /**
     * 用HS256算法创建token
     * @return
     */
    public static String genHsToken()
    {
        Algorithm algorithm = Algorithm.HMAC256("secret");
        String token = JWT.create().withIssuer("auth0").sign(algorithm);
        
        return token;
    }
    
    public static void verifyToken(String token)
    {
        Algorithm algorithm = Algorithm.HMAC256("secret");
        JWTVerifier jwtVerifier = JWT.require(algorithm).withIssuer("auth0").build();
        jwtVerifier.verify(token);
    }
    
    public static void main(String[] args)
    {
        String token = genHsToken();
        System.out.println(token);
        verifyToken(token);
    }
}
