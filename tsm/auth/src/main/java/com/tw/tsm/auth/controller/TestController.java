package com.tw.tsm.auth.controller;

import com.tw.tsm.base.MdcContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.tw.tsm.auth.feign.LogFeignClient;
import com.tw.tsm.feign.api.cipher.CipherFeignClient;

@RestController
@Slf4j
public class TestController
{
    
    @Autowired
    private RestTemplate restTemplate;
    
    @Autowired
    private CipherFeignClient authFeignClient;
    
    @Autowired
    private LogFeignClient logFeignClient;
    
    @RequestMapping(value = "/echo/{str}", method = RequestMethod.GET)
    public String echo(@PathVariable String str)
    {
        log.info("echo init begin..........tId：{}", MdcContext.getTraceId());
        //        return restTemplate.getForObject("http://cipher-service/echo/" + str, String.class);
        return authFeignClient.echo(str);
    }
    
    @RequestMapping(value = "/log/echo/{str}", method = RequestMethod.GET)
    public String logEcho(@PathVariable String str)
    {
        return logFeignClient.echo(str);
    }
    
}
