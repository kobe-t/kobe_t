package com.tw.tsm.feign.api.cipher;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.tw.tsm.base.constant.FeignConstant;
import com.tw.tsm.base.config.FeignConfiguration;

@FeignClient(name = FeignConstant.CIPHER_SERVICE, fallback = AuthFeignClientFallback.class, configuration = FeignConfiguration.class)
public interface CipherFeignClient
{
    @GetMapping(value = "/echo")
    String echo(@RequestParam(value = "str") String str);
    
}
