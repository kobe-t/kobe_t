package com.tw.tsm.feign.api.cipher;

import org.springframework.stereotype.Component;

@Component
public class AuthFeignClientFallback implements CipherFeignClient
{
    
    public String echo(String str)
    {
        return "fail";
    }
    
}
