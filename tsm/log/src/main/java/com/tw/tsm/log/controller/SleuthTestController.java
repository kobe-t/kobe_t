package com.tw.tsm.log.controller;

import com.tw.tsm.base.MdcContext;
import com.tw.tsm.feign.api.cipher.CipherFeignClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@Slf4j
public class SleuthTestController
{
    
    @Autowired
    private CipherFeignClient authFeignClient;
    
    @RequestMapping(value = "/echo/{str}", method = RequestMethod.GET)
    public void echo(@PathVariable String str)
    {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        String traceId = request.getHeader("X-B3-TraceId");
        String spanId = request.getHeader("X-B3-SpanId");
        log.info("echo init begin..........tId：{}", MdcContext.getTraceId());
        log.info("echo init begin..........tId：{},traceId {}, spanId {}", MdcContext.getTraceId(), traceId, spanId);
        log.info("echo init begin..........tId：{}", MdcContext.getTraceId());
    }
    
    @RequestMapping(value = "/cipher/echo/{str}", method = RequestMethod.GET)
    public String logEcho(@PathVariable String str)
    {
        return authFeignClient.echo(str);
    }
}
