package com.tw.tsm.log.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LogController
{
    
    @RequestMapping(value = "/log/echo/{string}", method = RequestMethod.GET)
    public String echo(@PathVariable String string) throws Exception
    {
        Thread.sleep(3000);
        return "Hello Nacos Discovery " + string;
    }
}
