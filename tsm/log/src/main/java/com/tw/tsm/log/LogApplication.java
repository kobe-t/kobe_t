package com.tw.tsm.log;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.tw.tsm.*"})
@ComponentScan(value = {"com.tw.tsm.*"})
public class LogApplication
{
    
    public static void main(String[] args)
    {
        SpringApplication.run(LogApplication.class, args);
    }
    
}