package com.tw.tsm.base;

import org.springframework.util.StringUtils;

import java.util.UUID;

public class MdcContext
{
    
    /** MDC上下文,存储tId */
    private static final ThreadLocal<String> CONTEXT = new ThreadLocal();
    
    /**
     * 获取tId，放入线程上下文中
     * @return
     */
    public static String getTraceId()
    {
        String tId = CONTEXT.get();
        if (StringUtils.isEmpty(tId))
        {
            CONTEXT.set(UUID.randomUUID().toString());
        }
        return CONTEXT.get();
    }
    
    /**
     * 清除线程上下文
     */
    public static void clear()
    {
        CONTEXT.remove();
    }
}
