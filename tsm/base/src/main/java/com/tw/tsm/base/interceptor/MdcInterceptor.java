package com.tw.tsm.base.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.web.servlet.HandlerInterceptor;

import com.tw.tsm.base.MdcContext;

@Slf4j
public class MdcInterceptor implements HandlerInterceptor
{
    
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
    {
        // 获取tId
        String tId = MdcContext.getTraceId();
        
        // 将tId放入MDC中，方便日志输出
        MDC.put("TraceId", tId);
        
        String traceId = MDC.get("traceId");
        String spanId = MDC.get("spanId");
        log.info("sleuth traceId:{}, spanId:{}", traceId, spanId);
        return true;
    }
    
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception
    {
        // 清除线程上下文
        MdcContext.clear();
        
        // 清楚MDC内容
        MDC.clear();
    }
}
