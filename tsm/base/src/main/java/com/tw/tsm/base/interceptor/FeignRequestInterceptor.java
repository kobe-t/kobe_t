package com.tw.tsm.base.interceptor;

import com.tw.tsm.base.MdcContext;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FeignRequestInterceptor implements RequestInterceptor
{
    
    /**
     * Called for every request. Add data using methods on the supplied {@link RequestTemplate}.
     *
     * @param template
     */
    @Override
    public void apply(RequestTemplate template)
    {
        String tId = "tId" + MdcContext.getTraceId();
        log.info("----------------传递tId:{}", tId);
        template.header("tId", tId);
    }
}
