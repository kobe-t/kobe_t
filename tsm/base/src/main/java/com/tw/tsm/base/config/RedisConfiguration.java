package com.tw.tsm.base.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisConfiguration
{
    
    /**
     * spring-boot-autoconfigure的RedisAutoConfiguration自动注册的RedisTemplate，使用的序列化器为默人的JdkSerializationRedisSerializer，序列化后生成的是不利于阅读的编码字符串。
     * 所以我们手动注册一个RedisTemplate，设置RedisConnectionFactory属性为spring-boot-autoconfigure的JedisConnectionConfiguration/LettuceConnectionConfiguration自动注册的RedisConnectionFactory，并设置序列化器为StringRedisSerializer。
     * 其实也可以直接在主启动类中使用@Autowired注入SpringBoot自动注册的RedisTemplate，并添加一个@PostConstruct的方法来修改它的序列化器为StringRedisSerializer。
     */
    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory)
    {
        final RedisTemplate<String, Object> template = new RedisTemplate<>();
        // 设置ConnectionFactory，SpringBoot会自动注册ConnectionFactory-Bean
        template.setConnectionFactory(redisConnectionFactory);
        
        // 设置序列化器为StringRedisSerializer（默认是 JdkSerializationRedisSerializer , java操作时会产生乱码）
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        template.setKeySerializer(stringRedisSerializer);
        template.setHashKeySerializer(stringRedisSerializer);
        template.setHashValueSerializer(stringRedisSerializer);
        
        template.setValueSerializer(new JdkSerializationRedisSerializer());
        template.afterPropertiesSet();
        return template;
    }

    /**
     * 配置StringRedisTemplate
     * @param factory
     * @return
     */
    @Bean
    public StringRedisTemplate stringRedisTemplate(
            RedisConnectionFactory factory) {
        StringRedisTemplate redisTemplate = new StringRedisTemplate();
        redisTemplate.setConnectionFactory(factory);
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        return redisTemplate;
    }
}
