package com.tw.tsm.base.response;

import lombok.Data;

/**
 * 响应基类
 */
@Data
public class BaseResponseDTO<T>
{
    /** 错误码 */
    private String code;
    
    /** 错误信息 */
    private String msg;
    
    /** 返回内容 */
    private T data;
}
