package com.tw.tsm.base.config;

import com.tw.tsm.base.interceptor.FeignRequestInterceptor;
import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignConfiguration
{
    
    @Bean
    public FeignRequestInterceptor feignRequestInterceptor()
    {
        return new FeignRequestInterceptor();
    }

    @Bean
    Logger.Level feignLoggerLevel()
    {
        return Logger.Level.FULL;
    }
}
