package com.tw.tsm.base.constant;

public class FeignConstant
{
    /** 加密服务 */
    public static final String CIPHER_SERVICE = "cipher-service";
    
    /** 日志服务 */
    public static final String LOG_SERVICE = "log-service";
}
