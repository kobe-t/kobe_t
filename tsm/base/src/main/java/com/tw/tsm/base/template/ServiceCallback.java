package com.tw.tsm.base.template;

/**
 * 模板回调函数
 *
 * @param <T>
 */
@FunctionalInterface
public interface ServiceCallback<T>
{
    default void check(T req)
    {
    }

    void doService(T req);
}
