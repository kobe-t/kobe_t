package com.tw.tsm.base.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 通用错误码
 */
@AllArgsConstructor
public enum ComErrorCode
{
    
    /** 缺少参数 */
    PARAM_MISS("E01000", "缺少参数"),
    /** 系统繁忙，请稍候重试 */
    SYSTEM_ERROR("E01999", "系统繁忙，请稍候重试");
    
    @Getter
    private String code;
    
    @Getter
    private String msg;
}
