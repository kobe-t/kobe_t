package com.tw.tsm.base.template;

import com.tw.tsm.base.exception.CheckedException;
import com.tw.tsm.base.exception.ComErrorCode;
import com.tw.tsm.base.request.BaseRequestDTO;
import com.tw.tsm.base.response.BaseResponseDTO;
import lombok.extern.slf4j.Slf4j;

/**
 * 模板方法类
 */
@Slf4j
public class WebTemplate
{
    
    /**
     * 模板方法
     * @param req 请求参数
     * @param res 响应参数
     * @param callback 回调方法
     * @param <T>
     * @param <R>
     */
    public static <T extends BaseRequestDTO, R extends BaseResponseDTO> void execute(T req, R res, ServiceCallback callback)
    {
        try
        {
            // 1:打印入参
            log.info("方法参数：{}", req);
            // 2:参数检验
            callback.check(req);
            // 3:业务方法
            callback.doService(req);
        }
        catch (Exception e)
        {
            // 4:异常处理
            handleException(e, res);
        }
        finally
        {
            log.info("处理结果：{}", res);
        }
    }
    
    /**
     * 异常处理
     * @param e
     * @param response
     */
    private static void handleException(Exception e, BaseResponseDTO response)
    {
        if (e instanceof CheckedException)
        {
            CheckedException ex = (CheckedException)e;
            ex.setCode(ex.getCode());
            ex.setMsg(ex.getMsg());
            return;
        }
        // 设置默认异常
        response.setCode(ComErrorCode.SYSTEM_ERROR.getCode());
        response.setMsg(ComErrorCode.SYSTEM_ERROR.getMsg());
    }
}
