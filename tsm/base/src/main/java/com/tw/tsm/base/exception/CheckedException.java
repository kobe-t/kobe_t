package com.tw.tsm.base.exception;

import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * 检验异常
 */
@Data
@RequiredArgsConstructor
public class CheckedException extends RuntimeException
{
    
    /** 错误码 */
    private String code;
    
    /** 错误信息 */
    private String msg;
}
